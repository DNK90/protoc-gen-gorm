module bitbucket.org/DNK90/protoc-gen-gorm

require (
	github.com/dgrijalva/jwt-go v0.0.0-20180719211823-0b96aaa70776
	github.com/dnk90/gorm v1.9.14-dnk90.1
	github.com/ghodss/yaml v1.0.0
	github.com/gogo/protobuf v1.0.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.4.1
	github.com/google/uuid v0.0.0-20161128191214-064e2069ce9c
	github.com/grpc-ecosystem/go-grpc-middleware v0.0.0-20180522105215-e9c5d9645c43
	github.com/grpc-ecosystem/grpc-gateway v1.4.1
	github.com/grpc-ecosystem/grpc-opentracing v0.0.0-20180507213350-8e809c8a8645
	github.com/infobloxopen/atlas-app-toolkit v0.0.0-20180810144038-3237ef051031
	github.com/infobloxopen/go-trees v0.0.0-20180608122459-a1e500cde93c
	github.com/infobloxopen/themis v0.0.0-20180709114455-d62bb103665c
	github.com/jinzhu/gorm v1.9.14
	github.com/jinzhu/inflection v1.0.0
	github.com/lib/pq v1.1.1
	github.com/opentracing/opentracing-go v1.0.2
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.0.5
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd
	golang.org/x/text v0.3.2
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
	google.golang.org/protobuf v1.23.1-0.20200526195155-81db48ad09cc
	gopkg.in/yaml.v2 v2.2.2
)

go 1.13
