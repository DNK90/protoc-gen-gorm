package main

import (
	p "bitbucket.org/DNK90/protoc-gen-gorm/plugin"
	"github.com/gogo/protobuf/vanity/command"
)

func main() {
	plugin := &p.OrmPlugin{}
	response := command.GeneratePlugin(command.Read(), plugin, ".pb.gorm.go")
	//plugin.CleanFiles(response)
	command.Write(response)
}
